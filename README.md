**Order Pizza App using MVVM**

This is a sample app that utilizes calling of POST and GET HTTP requests, UIKit, and Swift. The source code, demonstrates a simple implementation of MVVM structure in Swift language.

---

## API

The API used is from a NodeJS with ExpressJS application served in HerokuApp (NodeJS project can be found here: https://github.com/senducusin/jarvis-nodejs.git).  
- Display all orders: (GET) http://jarvis-nodejs.herokuapp.com/api/pizza/orders  
- Display order by ID: (GET) http://jarvis-nodejs.herokuapp.com/api/pizza/orders  
- Add an order: (POST) http://jarvis-nodejs.herokuapp.com/api/pizza/order  

*Note: Server is strictly for testing. Orders are only stored in the memory and NOT stored in a database. Everytime the server is restarted, the Orders will be erased.*

## Features

- Display orders in a list view (Name + Pizza type)
- Accepts pizza orders and send it to the API

