//
//  OrderViewModel.swift
//  OrderPizzaApp
//
//  Created by Jansen Ducusin on 2/4/21.
//

import Foundation

struct OrderViewModel {
    let order:  Order
}

extension OrderViewModel{
    var name:   String{
        return self.order.name
    }
    
    var email:  String{
        return self.order.email
    }
    
    var type:   String{
        return self.order.type.rawValue.capitalized
    }
    
    var size:   String{
        return self.order.size.rawValue.capitalized
    }
}
