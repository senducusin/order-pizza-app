//
//  OrderListViewModel.swift
//  OrderPizzaApp
//
//  Created by Jansen Ducusin on 2/4/21.
//

import Foundation

class OrderListViewModel{
    var ordersViewModel: [OrderViewModel]
    
    init() {
        self.ordersViewModel = [OrderViewModel]()
    }
}

extension OrderListViewModel{
    var numberOfSections: Int{
        return 1
    }
    
    func numberOfRowsInSection(_ section:   Int) -> Int{
        return self.ordersViewModel.count
    }
    
    func orderAtIndex(at index:   Int) -> OrderViewModel{
        return self.ordersViewModel[index]
    }
}
