//
//  AddOrderViewModel.swift
//  OrderPizzaApp
//
//  Created by Jansen Ducusin on 2/5/21.
//

import Foundation

struct AddOrderViewModel {
    var name:   String?
    var email:  String?
    var selectedPizzaType:  String?
    var selectedPizzaSize:  String?
    
    var types:  [String]{
        return PizzaType.allCases.map{$0.rawValue.capitalized}
    }
    
    var sizes:  [String]{
        return PizzaSize.allCases.map{$0.rawValue.capitalized}
    }
}

extension AddOrderViewModel{
    var pizzaTypesNumberOfSections:Int{
        return 1
    }
    
    func pizzaTypesNumberOfRowsInSection(_ section: Int) -> Int{
        return self.types.count
    }
    
    func pizzaTypeAtIndex(at index: Int) -> String{
        return self.types[index]
    }
}
