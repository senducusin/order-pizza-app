//
//  OrdersTableViewController.swift
//  OrderPizzaApp
//
//  Created by Jansen Ducusin on 2/3/21.
//

import Foundation
import UIKit

class OrdersTableViewController:    UITableViewController{
    
    var orderViewListModel = OrderListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateOrders()
    }
    
    private func populateOrders(){
        
        WebService().load(resource: Order.all){ [weak self] result in
            switch result {
                case .success(let orders):
                    self?.orderViewListModel.ordersViewModel = orders.map(OrderViewModel.init)
                    self?.tableView.reloadData()
                case .failure(let error):
                    print(error)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let navigationController = segue.destination as? UINavigationController,
              let addPizzaOrderViewController = navigationController.viewControllers.first as? AddOrderViewController else {
            fatalError("Error performing segue")
        }
        
        addPizzaOrderViewController.delegate = self
    }

}

extension OrdersTableViewController:    AddPizzaOrderDelegate{
    func addPizzaOrderViewControllerDidSave(controller: UIViewController) {
        controller.dismiss(animated: true, completion: nil)
        populateOrders()
    }
    
    func addPizzaOrderViewControllerDidClose(controller: UIViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension OrdersTableViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return orderViewListModel.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderViewListModel.ordersViewModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let order = orderViewListModel.orderAtIndex(at: indexPath.row)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell")!
        
        cell.textLabel?.text = order.name
        cell.detailTextLabel?.text = order.type
        return cell
    }
}
