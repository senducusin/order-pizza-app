//
//  AddOrderViewController.swift
//  OrderPizzaApp
//
//  Created by Jansen Ducusin on 2/3/21.
//

import Foundation
import UIKit

protocol    AddPizzaOrderDelegate{
    func addPizzaOrderViewControllerDidSave(controller:UIViewController)
    func addPizzaOrderViewControllerDidClose(controller:UIViewController)
}

class AddOrderViewController:   UIViewController{
    
    var delegate:   AddPizzaOrderDelegate?
    
    private var viewModel = AddOrderViewModel()
    private var pizzaSizesSegmentedControl: UISegmentedControl!
    
    @IBOutlet weak var tableView:   UITableView!
    @IBOutlet weak var nameTextField:   UITextField!
    @IBOutlet weak var emailTextField:  UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI(){
        self.pizzaSizesSegmentedControl = UISegmentedControl(items: self.viewModel.sizes)
        self.pizzaSizesSegmentedControl.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(self.pizzaSizesSegmentedControl)
        
        self.pizzaSizesSegmentedControl.topAnchor.constraint(equalTo: self.tableView.bottomAnchor, constant: 20).isActive = true
        
        self.pizzaSizesSegmentedControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        self.pizzaSizesSegmentedControl.selectedSegmentIndex = 0
        
    }
    
    @IBAction func close(){
        if let delegate = self.delegate {
            delegate.addPizzaOrderViewControllerDidClose(controller: self)
        }
}
    
    @IBAction func save(){
        let name = self.nameTextField.text
        let email = self.emailTextField.text
        
        let selectedSizeIndex = self.pizzaSizesSegmentedControl.selectedSegmentIndex
        let selectedSize = self.pizzaSizesSegmentedControl.titleForSegment(at: selectedSizeIndex)
        
        guard let indexPath = self.tableView.indexPathForSelectedRow else{
            fatalError("No selected type of Pizza")
        }
        
        self.viewModel.name = name
        self.viewModel.email = email
        self.viewModel.selectedPizzaType = self.viewModel.pizzaTypeAtIndex(at: indexPath.row)
        self.viewModel.selectedPizzaSize = selectedSize
        
        WebService().load(resource: Order.create(viewModel: self.viewModel)){ result in
            switch result {
            case .success(_):
                if let delegate = self.delegate {
                    DispatchQueue.main.async {
                        delegate.addPizzaOrderViewControllerDidSave(controller: self)
                    }
                }
            
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension AddOrderViewController:   UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.pizzaTypesNumberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.pizzaTypesNumberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddOrderTableViewCell",for: indexPath)

        cell.textLabel?.text = self.viewModel.pizzaTypeAtIndex(at: indexPath.row)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
}
