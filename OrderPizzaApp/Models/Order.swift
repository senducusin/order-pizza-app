//
//  Order.swift
//  OrderPizzaApp
//
//  Created by Jansen Ducusin on 2/4/21.
//

import Foundation

enum PizzaType: String, Codable, CaseIterable{
    case angusBurgerPizza = "Angus Burger Pizza"
    case bellyBuster = "Belly Buster"
    case classicCheese = "Classic Cheese"
    case hawaiianDelight = "Hawaiian Delight"
    case pepperoni = "Pepperoni"
}

enum PizzaSize: String, Codable, CaseIterable{
    case regular
    case large
    case party
}

struct Order:   Codable {
    let name:   String
    let email:  String
    let type:   PizzaType
    let size:   PizzaSize
}

extension Order{
    init?(_ viewModel: AddOrderViewModel){
        guard let name = viewModel.name,
              let email = viewModel.email,
              let selectedType = PizzaType(rawValue: viewModel.selectedPizzaType!),
              let selectedSize = PizzaSize(rawValue: viewModel.selectedPizzaSize!.lowercased())
        else {
            return nil
        }
        
        self.name = name
        self.email = email
        self.type = selectedType
        self.size = selectedSize
    }
}

extension Order{
    
    static var all: Resource<[Order]> = {
        guard  let url = URL(string: "http://jarvis-nodejs.herokuapp.com/api/pizza/orders") else {
            fatalError("URL is incorrect")
        }
        
        return Resource<[Order]>(url: url)
    }()
    
    static func create(viewModel: AddOrderViewModel) -> Resource<Order?>{
        let order = Order(viewModel)
        
        guard let url = URL(string: "http://jarvis-nodejs.herokuapp.com/api/pizza/order") else {
            fatalError("URL is incorrect")
        }
        
        guard let data = try? JSONEncoder().encode(order) else {
            fatalError("Error encoding order")
        }
        
        var resource = Resource<Order?>(url: url)
        resource.httpMethod = .post
        resource.body = data
        
        return resource
    }
}
